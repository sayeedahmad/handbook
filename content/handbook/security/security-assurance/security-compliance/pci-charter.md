---
title: "PCI Charter"
controlled_document: true
---

## Purpose

This charter establishes the governance framework and organizational structure for maintaining compliance with the Payment Card Industry Data Security Standard (PCI DSS) requirements. It defines roles, responsibilities, and accountability measures to ensure the security of GitLab's SaaS offerings as a Service Provider.

## Program Governance

The Chief Information Security Officer, supported by the Security Assurance team, maintains ultimate accountability for PCI DSS compliance and are responsible for:

- Reviewing and approving the PCI DSS charter annually
- Reviewing and approving PCI DSS scope every 6 months
- Ensuring adequate resource allocation
- Overseeing risk assessment and management
- Coordinating necessary remediation activities

## Communication and Reporting

Results and status are communicated to executive management.
