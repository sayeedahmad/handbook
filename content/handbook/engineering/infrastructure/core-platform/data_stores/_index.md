---
title: Core Platform:Data Stores Stage
---

## Vision

Develop the tooling and frameworks to support the scalability and reliability of GitLab's usage of data stores such as PostgreSQL and ElasticSearch.

## Teams

* [Database](/handbook/engineering/infrastructure-platforms/data-access/database-framework/)
* [Database Reliability](/handbook/engineering/infrastructure-platforms/data-access/database-framework-reliability/)
* [Cloud Connector](/handbook/engineering/infrastructure/core-platform/data_stores/cloud-connector/)
