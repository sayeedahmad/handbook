---
title: "Developer Experience"
description: "Developer Experience is a newly formed group, born from the strategic merger of the Engineering Productivity team and the Test Platform sub-department. This exciting combination allows us to take a holistic approach to delivering cutting-edge Platform capabilities."
---

## Mission

Our mission is to empower developers to focus on innovation, build, and deliver high-quality products to our customers. We aim to achieve this through:

1. State-of-the-art developer tooling
2. Robust and reliable test infrastructure
3. Data-driven analysis for informed decision-making
4. Streamlined release governance
5. Comprehensive performance validation

## Team Structure

Infrastructure Platforms Department structure is documented [here](/handbook/engineering/infrastructure-platforms/#organization-structure).

### Developer Experience group structure

```mermaid
graph TD
    DE[Developer Experience group]
    click DE "/handbook/engineering/infrastructure-platforms/developer-experience"

    DE --> DA[Development Analytics]
    click DA "handbook/engineering/infrastructure-platforms/developer-experience"
    DE --> DT[Developer Tooling]
    click DT "/handbook/engineering/infrastructure-platforms/developer-experience/developer-tooling-team"
    DE --> FR[Feature Readiness]
    click FR "handbook/engineering/infrastructure-platforms/developer-experience"
    DE --> PE[Performance Enablement]
    click PE "handbook/engineering/infrastructure-platforms/developer-experience"
    DE --> TG[Test Governance]
    click TG "handbook/engineering/infrastructure-platforms/developer-experience"
```

## Team Members

### Management team

{{< team-by-manager-role role="Director, Test Platform" >}}

### Individual contributors

The following people are members of the [Development Analytics group](./development-analytics/):

{{< team-by-manager-slug "ghosh-abhinaba" >}}

The following people are members of the [Developer Tooling group](developer-tooling-team):

{{< team-by-manager-slug "vincywilson" >}}

The following people are members of the [Feature Readiness group](feature-readiness-team):

{{< team-by-manager-slug "ksvoboda" >}}

The following people are members of the [Performance Enablement group](performance-enablement-team):

{{< team-by-manager-slug "ksvoboda" >}}

The following people are members of the [Test Governance group](test-governance-team):

{{< team-by-manager-slug "kkolpakova" >}}
