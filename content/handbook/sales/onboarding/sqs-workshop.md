---
title: "Sales Quick Start (SQS) Workshop"
---

## Sales Quick Start Workshop

- NOTE: The Sales Quick Start (SQS) Workshop is MANDATORY for all new Sales Team Members. If you must cancel for any reason, please be sure to obtain Regional Director approval in writing.
- After completing the virtual, self-paced Sales Quick Start learning path, new sales team members (along with new Solution Architects, Customer Success Managers, and Sales Development Rep) participate in an interactive in-person workshop where participants practice applying this new knowledge in fun and challenging ways including mock calls, role plays, and group activities
  - As a result, new sales team members exit Sales Quick Start more confident and capable in their ability to lead an effective discovery call with a customer or prospect
  - Participants also benefit by establishing a community of similarly-tenured peers and more experienced mentors and colleagues to support each others' growth and development at GitLab
- **Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery)**
  - Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way
- Future iteration of this process will define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!

## Sales Quick Start In-Person Workshop Agenda

This [SQS 2024 Agenda sheet](https://docs.google.com/spreadsheets/d/1f64fZCKbrz7JEydEIkUeGZ16nQuLxNgD6RXEM2zEgws/edit?usp=sharing) contains the most up to date agenda for our in-person Sales Quick Start Workshop. Please check the tabs at the bottom of this document to see each day's agenda. This agenda is subject to change based on the individual needs of the class and the availability of SMEs, but we will make every effort to surface those changes in this document.

## Sales Quick Start Remote Agenda

### SQS 44 - February 2025

- Workshop times quoted are United States Eastern Standard Time (New York) (UTC -5)

| DATE | START TIME | END TIME | ACTIVITY | SME ASSIGNED |
|------|------------|----------|----------|--------------|
| Feb 6, 2025 | 10:00a ET | 11:00a ET | Welcome Call | Field Enablement |
| Feb 10, 2025 | 10:30a ET | 10:50a ET | Operational Excellence: Into to Gitlab Field Security | Field Security |
| Feb 10, 2025 | 11:00a ET | 11:50a ET | Solution Focus: Intro to Competition | Product Marketing |
| Feb 11, 2025 | 10:00a ET | 11:20a ET | Intro to Ecosystem Sales | Ecosystem Team |
| Feb 12, 2025 | 10:00a ET | 10:20a ET | Operational Excellence: Intro to GitLab Legal | Legal Team |
| Feb 12, 2025 | 10:30a ET | 11:20a ET | Discussion: Professional Services | Professional Services |
| Feb 12, 2025 | 11:30a ET | 12:20p ET | Operational Excellence: Working with Sales Dev | Marketing Enablement |
| Feb 13, 2025 | 10:00a ET | 10:50a ET | Customer Focus: Customer Success Overview | Customer Success Team |
| Feb 14, 2025 | 11:00a ET | 11:30a ET | Intro to Sales Ops + Deal Desk | RSOE Team |
| Feb 14, 2025 | 10:00a ET | 10:50a ET | GitLab Product & Personas Overview | Field Enablement   |
| Feb 13, 2025 | 11:30a ET | 11:55a ET | Renewals Manager Overview | Renewals Team  |
| Feb 17, 2025 | 10:30a ET | 11:50a ET | Command of the Message: Essential Questions Exercise | Field Enablement |
| Feb 18, 2025 | 10:30a ET | 11:50a ET | Command of the Message: Value Card Exercise | Field Enablement |
| Feb 19, 2025 | 10:30a ET | 11:50a ET | Command of the Message: Discovery Question Exercise | Field Enablement |
| Feb 20, 2025 | 10:30a ET | 11:50a ET | Command of the Message: Differentiator Exercise | Field Enablement |
| Feb 21, 2025 | 10:00a ET | 10:50a ET | Command of the Message: MEDDPPICC & Breakout Call Prep | Field Enablement |
| Feb 24 - 27, 2025 | 10:00a ET | 10:50a ET | Mock Discovery Calls | Mock Customers |
